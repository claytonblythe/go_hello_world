package main

import (
	"fmt"

	"gitlab.com/claytonblythe/go_hello_world/morestrings"
)

func main() {
	fmt.Println(morestrings.ReverseRunes("!oG ,olleH"))
}
